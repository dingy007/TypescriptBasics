console.log('*** INTERFACES ***');

interface Person {
    firstName: string;
    lastName: string;
}

function displayName(user: Person) {
    console.log(user.firstName + ' ' + user.lastName);
}

let person = { firstName: 'Jon', lastName: 'Doe' };
displayName(person);