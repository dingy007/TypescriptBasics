console.log('*** CLASS ***');

interface EmployeeInterface {
    firstName: string;
    // lastName: string;
    // phoneNumber: number;
    status();
}

class Employee implements EmployeeInterface {
    public firstName: string; // public is optional
    private lastName: string; // only class level
    protected phoneNumber: number; // only inherited classes can see this.

    constructor(firstName: string, lastName?: string, phoneNumber?: number) {
        if (lastName == undefined) {
            lastName = '';
        }
        if (phoneNumber == undefined) {
            phoneNumber = 0;
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    public getLastName():string {
        return this.lastName;
    }
    public getPhoneNumber():number {
        return this.phoneNumber;
    }

    public status() {
        console.log('Employee: ', this.firstName, ' created successfully.');
    }
}

let emp:Employee = new Employee('John', 'Doe');
console.log('New Employee First Name: ', emp.firstName);
console.log('New Employee Last Name: ', emp.getLastName());
console.log('New Employee Phone: ', emp.getPhoneNumber());
emp.status();

class Developer extends Employee {
    private id:number;

    constructor(id: number, firstName: string, phoneNumber: number, lastName?: string) {
        super(firstName, lastName, phoneNumber);
        this.id = id;
    }

    public status() {
        console.log('Developer: ', this.id, ' was created successfully.');
    }
}

let dev1:Employee = new Developer(1,'Willy', 123456);
console.log('New Developer First Name: ', dev1.firstName);
console.log('New Developer Last Name: ', dev1.getLastName());
console.log('New Developer Phone: ', dev1.getPhoneNumber());
dev1.status();