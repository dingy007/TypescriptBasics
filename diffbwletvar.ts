console.log('*** DIFFERENCE BETWEEN LET AND VAR EXPLAINED ***');
// Difference between let/var 1:
f();
function f() {
    var var1 = "X";
    var var1 = "Re-declared";
    let let2 = "Y";
    // let let2 = "Z";
    console.log("VAR1:", var1);
    console.log("VAR2:", let2);

}
// console.log("VAR1 outside function f:", var1);
// console.log("VAR2 outside function f:", let2);
// Difference between let/var 2:
countDown();
// countDown2();
function countDown() {
    for (let i = 0; i < 10; i++) {
        // console.log('Let countup: ', i);
        setTimeout(function () { console.log(i); }, 100 * i);
    }
}
function countDown2() {
    for (var i = 0; i < 10; i++) {
        // console.log('Let countup: ', i);
        setTimeout(function () { console.log(i); }, 100 * i);
        // IIFE: Immediately Invoked Function Expression
        /* 
        (function (i) {
            setTimeout(function () { console.log(i); }, 100 * i);
        })(i);
 */
    }
}