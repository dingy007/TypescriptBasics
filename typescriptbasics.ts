console.log('*** DIFFERENCE BETWEEN LET AND VAR EXPLAINED ***');

class MyClass {
    public static main() {
        var myString: String = "This is" + " my String"; // variable declared with type and value
        var myString: String; // String type without a value
        let myAny:any = 1234; //Any type
        let myBool: boolean = false;
        let myNum: number = 134.45;

        let myString2: String = "Let String";
        console.log('Variable type:', typeof myAny);
        console.log('MyString2: ', myString2);
        console.log('myNum:', myNum);

        let strArray:string[] = ['item1', 'item1=2'];
        let strArray2:Array<string>;
        strArray2 = ['item3','item4'];
        console.log('strArray:', strArray);
        console.log('strArray2:', strArray2);

        let numStrTuple:[number, string];
        numStrTuple = [1,'item5'];
        console.log('numStrTuple:', numStrTuple);

        let myVoid: void = null;//undefined
        console.log('myVoid:', myVoid);

        enum Item {book, pencil, eraser};
        let c:Item = Item.eraser;
        console.log('enum c:', c);

    }
}
MyClass.main();