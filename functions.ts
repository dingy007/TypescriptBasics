console.log('*** FUNCTIONS ***');


function concatStrings(str1, str2: string): string {
    return str1.concat(str2);
    // return 1;
}

let str3 = concatStrings('Concat', 'String')
console.log('Calling function:', str3);

function getName(firstName: string, lastName?: string): string {
    if (lastName == undefined) {
        return firstName;
    }
    return firstName + ' ' + lastName;
}

console.log('getName', getName('hewlett'));
console.log('getName', getName('hewlett', 'packard'));

let object = {
    id: 'abcd',
    status: function () {
        setTimeout(function () { // anon function
            console.log('Object: Id of this: ', this.id), 100
        });
    }
}
object.status();

let object2 = {
    id: 'efgh',
    status: function () {
        setTimeout(() => console.log('Object2: Id of this: ', this.id), 100);
    }
}
object2.status();